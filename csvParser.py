import pandas
import os
import mathsPlots

def openFile():
    ficheros = os.listdir(".")
    i = 0
    ficherosCSV = []
    for file in ficheros:
        if not file.startswith('.') and file.endswith('.csv'):
            print(str(i+1) + '- ' + file)
            i = i + 1
            ficherosCSV.append(file)
    itemNum = input("Introduzca el número de fichero")
    file_path = os.path.join('.', ficherosCSV[int(itemNum) - 1])
    dataset = pandas.read_csv(file_path, names=['Cups', 'Fecha', 'Hora', 'kwh', 'tipo medicion', 'tipo'])
    print(dataset.describe())
    return dataset


def menuPrint(datasetArray):
    while (1):
        print("Menu\n 1. Histogram Plot\n 2. Boxplot\n 3. Separate dataset by weeks\n 4. Separate dataset by day\n 5. Open another file\n 6.Análisis de series temporales\n7.Análisis de Entropias\n8.Prediccion\n9.Comparar series temporales\n10.Estadística\n11.Seasonal ESD")
        text = input()
        if text == '1':
            mathsPlots.histogram_plot(datasetArray)
        if text == '2':
            mathsPlots.boxplot(datasetArray)
        if text == '3':
            datos = mathsPlots.parseData(datasetArray)
            dataSeparated = mathsPlots.dataIntoWeeks(datos)
            menuByWeeks(dataSeparated)
        if text == '4':
            datos = mathsPlots.parseData(datasetArray)
            dataSeparated = mathsPlots.dataIntoDays(datos)
            menuByDays(dataSeparated)
        if text == '5':
            newDataset = openFile()
            print(datasetArray.__len__())
            datasetArray.append(newDataset)
            print(datasetArray.__len__())
        if text == '6':
            year = mathsPlots.dataIntoTime(datasetArray)
            freq = input('Introduzca la frecuencia: ')
            mathsPlots.seriesDataAnalisis(year,int(freq))
        if text == '7':
            windowSize = input("Window entropy calculation size: ")
            period = input('Period: ')
            mathsPlots.entropyCalculation(datasetArray,int(windowSize), int(period))
        if text== '8':
            year = mathsPlots.dataIntoTime(datasetArray)
            mathsPlots.testPrediction(year)
        if text == '9':
            newDataset = openFile()
            data =[]
            data.append(newDataset)
            for dataset in datasetArray:
                mathsPlots.compareTimeSeries(dataset,newDataset)
        if text == '10':
            newDataset = pandas.DataFrame()
            for dataset in datasetArray:
                newDataset = newDataset.append(dataset)
            print(newDataset.describe())
            print(newDataset.sum())
        if text == '11':
            year = mathsPlots.dataIntoTime(datasetArray)
            mathsPlots.seasonalESD(year)
def menuByWeeks(datos):
    while (1):
        print("Menu by weeks\n 1. Histogram Plot\n 2. Boxplot\n 3.Return to main menu")
        text = input()
        if text == '1':
            mathsPlots.histogram_plot_by_Weeks(datos)
        if text == '2':
            mathsPlots.boxplot_by_weeks(datos)
        if text == '3':
            break


def menuByDays(datos):
    while(1):
        print("Menu by days\n 1. View histogram plot\n 2. Boxplot of all days\n 3. Histogram of day of week \n 5. Return to main menu")
        text = input()
        if text == '1':
            mathsPlots.histogram_plot_by_Weeks(datos)
        if text == '2':
            mathsPlots.boxplot_by_weeks(datos)
        if text == '3':
            day = input('Input the day of week (0-6 where 0 is Monday and 6 is Sunday)')
            day = int(day)
            mathsPlots.histogramByDay(datos,day)
            int(day)
        if text == '4':
            mathsPlots.boxplotByDay(datos)
        if text == '5':
            break


dataset = openFile()
datasetArray = []
datasetArray.append(dataset)
menuPrint(datasetArray)


