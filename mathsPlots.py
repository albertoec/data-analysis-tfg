import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from sklearn.cluster import KMeans
import datetime
import numpy
import pandas
import pandas as pd
import datetime
import statsmodels.api as sm
from entropy import *
from statsmodels.tsa.stattools import acf, pacf
import itertools
import warnings
import matplotlib.cm as cm
import dtwco
from dtw import dtw
import sesd

def append_series(datasetArray):
    newDataset =[]
    for dataset in datasetArray:
        print(dataset['kwh'])
def histogram_plot(datasetArray):
    newDataset =pandas.DataFrame()
    for dataset in datasetArray:
        newDataset=newDataset.append(dataset)
    fig=plt.figure(figsize=(9, 8))
    sns.distplot(newDataset['kwh'], color='g',bins=40, hist_kws={'alpha': 0.4});
    plt.xlabel('kWh')
    plt.show()
    fig.savefig("histograma_completo.pdf", bbox_inches='tight')


def histogram_plot_by_Weeks(arrayCups):
    plt.figure(figsize=(9, 8))
    for dataSeparated in arrayCups:
        simpleWeeks = []
        for week in dataSeparated:
            simpleWeek = []
            for hour in week:
                arrayHoraKwh = []
                arrayHoraKwh.append(hour[0])
                fecha = hour[2].split('/') # dia, mes, anio
                time = datetime.datetime(int(fecha[2]),int(fecha[1]),int(fecha[0]),int(hour[1])) #anio mes dia hora
                arrayHoraKwh.append(time)
                simpleWeek.append(arrayHoraKwh)
            simpleWeeks.append(simpleWeek)
        for plotWeek in simpleWeeks:
            consumo = [i[0] for i in plotWeek]
            hora = [i[1] for i in plotWeek]
            horasPlot = matplotlib.dates.date2num(hora)
            fig = plt.figure(1)
            plt.plot_date(horasPlot, consumo,'b')
            plt.rcParams.update({'font.size': 22})
            plt.show()
            fig.savefig("consumo_semana.pdf", bbox_inches='tight')


def boxplot(datasetArray):
    fig = plt.figure(figsize=(10, 6))
    plot = []
    labels = []
    for dataset in datasetArray:
        plot.append(dataset['kwh'])
        labels.append(dataset['Fecha'][0].split('/')[2])
    plt.boxplot(plot,labels=labels,showmeans=True,showfliers=True)
    plt.ylabel('kWh')
    plt.xticks(rotation=45)
    plt.show()
    fig.savefig("boxplot.pdf", bbox_inches='tight')


def boxplot_by_weeks(arrayCups):
    fig = plt.figure(figsize=(10, 6))
    plot = []
    labels = []
    for dataSeparated in arrayCups:
        for semanas in dataSeparated:
            consumo = [i[0] for i in semanas]
            fecha = [i[2] for i in semanas]
            try:
                labels.append(fecha[0])
                plot.append(consumo)
            except:
                continue
        plt.boxplot(plot, labels=labels,showmeans=True, showfliers=True)
        plt.xticks(rotation=90)
        plt.show()

def kmeans(dataset):
    fig = plt.figure(figsize=(10, 6))
    kmean = KMeans(n_clusters=3).fit(dataset)
    #plt.scatter(dataset['kwh'], X['']
    #labels = kmean.predict(dataset)

def parseData(datasetArray):
    array =[]
    for dataset in datasetArray:
        cup = []
        for index, row in dataset.iterrows():
            dato=[]
            dato.append(row['kwh'])
            dato.append(row['Hora'].split(':')[0])
            dato.append(row['Fecha'])
            cup.append(dato)
        array.append(cup)
    return array


def dataIntoWeeks(separateData):
    cupsIntoWeeks = []
    for array in separateData:
        anios = []
        week = []
        semana = 0
        semanaActual = datetime.datetime(1,1,1)
        for dato in array:
            dia, mes, anio = dato[2].split('/')
            fecha = datetime.datetime(int(anio),int(mes),int(dia))
            dayOfWeek = fecha.weekday()
            if semana > 0 and dayOfWeek == 0: #new week
                if week:
                    anios.append(week)
                    week = []
                    semana=0
                semana= semana +dayOfWeek
            else:
                week.append(dato)
                semana = semana + dayOfWeek
        anios.append(week)
        cupsIntoWeeks.append(anios)
    return cupsIntoWeeks

def dataIntoDays(separateData):
    cupsIntoWeeks = []
    days = []
    for array in separateData:
        lastDay = ''
        actualDay = ''
        day = []
        first = 0
        for data in array:
            actualDay = data[2]
            if lastDay != actualDay and first == 1: # new day
                days.append(day)
                day = []
                lastDay = actualDay
                day.append(data)
            else:
                day.append(data)
                lastDay = data[2]
                first = 1
        days.append(day)
        test = []
        test.append(days)
        return test

def histogramByDay(arrayCups, dayOfWeek):
    for dataSeparated in arrayCups:
        simpleWeeks = []
        for week in dataSeparated:
            simpleWeek = []
            for hour in week:
                arrayHoraKwh = []
                arrayHoraKwh.append(hour[0])
                fecha = hour[2].split('/') # dia, mes, anio
                time = datetime.datetime(int(fecha[2]),int(fecha[1]),int(fecha[0]),int(hour[1])) #anio mes dia hora
                if dayOfWeek != time.weekday():
                    continue
                arrayHoraKwh.append(time)
                simpleWeek.append(arrayHoraKwh)
            if(simpleWeek.__len__() != 0):
                simpleWeeks.append(simpleWeek)
        for plotWeek in simpleWeeks:
            consumo = [i[0] for i in plotWeek]
            hora = [i[1] for i in plotWeek]
            horasPlot = matplotlib.dates.date2num(hora)
            plt.plot_date(horasPlot, consumo,'b')
            plt.show()
def boxplotByDay(arrayCups):
    for dataSeparated in arrayCups:
        simpleWeeks = []
        dias = []
     #   dias = [None] * 7
       # for i in range(0,7):
       #     dias[i]=[]
        for week in dataSeparated:
            simpleWeek = []
            for hour in week:
                arrayHoraKwh = []
                arrayHoraKwh.append(hour[0])
                fecha = hour[2].split('/') # dia, mes, anio
                time = datetime.datetime(int(fecha[2]),int(fecha[1]),int(fecha[0]),int(hour[1])) #anio mes dia hora
                arrayHoraKwh.append(time.weekday())
                #dias[time.weekday()].append(arrayHoraKwh)
                dias.append(arrayHoraKwh)
        #kwh = [i[0] for i in dias]
      #  dia = [i[1] for i in dias]
      #  print(dias)
       # print(dia)
        fig = plt.figure(figsize=(10, 6))
        plot = []
        labels = ['Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo']
        data1 = []
        data2 = []
        for i in range(0,7):
            for data in dias:
                if(data[1]==i):
                    data1.append(data[0])
            data2.append(data1)
            data1 = []

        print(data2)
        plt.boxplot(data2,labels=labels,showmeans=True,showfliers=True)
        plt.ylabel('kWh')
        #plt.xticks(rotation=45)
        plt.show()
        fig.savefig("boxplot_diasSemana.pdf", bbox_inches='tight')


def dataIntoTime(separateData):
    year = []
    for dataset in separateData:
        for index, row in dataset.iterrows():
            dato = []
            hora = row['Hora'].split(':')
            fecha = row['Fecha'].split('/') # dia, mes, anio
            time = datetime.datetime(int(fecha[2]), int(fecha[1]), int(fecha[0]), int(hora[0]))  # anio mes dia hora
            #dato.append(row['Cups'])
            dato.append(time)
            if(row['kwh']<=0):
                row['kwh']=1
            dato.append(row['kwh'])
            year.append(dato)
    data= pandas.DataFrame(year,columns=['time','kwh']) #  data= pandas.DataFrame(year,columns=['cups','time','kwh'])
    data.set_index('time', inplace=True, drop=True)
    #data = data.resample('24H').sum() # data = data.resample('W-MON').sum()
    #4data = data.resample('B-MON').sum()
    return data

def entropyCalculation(separateData,windowSize, period):
    data =  []
    for dataset in separateData:
        i=0
        window = []
        for x in range(0, dataset['kwh'].__len__(), period):
            window=[]
            #print('x value'+str(x))
            fecha = dataset['Fecha'][x].split('/') # dia, mes, anio
            time = datetime.datetime(int(fecha[2]), int(fecha[1]), int(fecha[0]))  # anio mes dia hora
            window.append(time)
            for y in range(-windowSize+x, windowSize+x):
                if(y < 0 or y >= dataset['kwh'].__len__()):
                    continue
                #print('y value'+str(y)+'\n\n')
                window.append(dataset['kwh'][y])
            data.append(window)
    entropy =[]
    for window in data:
        #print(window[0])
        simpleDato = []
        simpleDato.append(window[0])
        simpleDato.append(app_entropy(window[1:], order=2, metric='chebyshev')) # The value of r is set to 0.2∗std(x).
        entropy.append(simpleDato)
    data= pandas.DataFrame(entropy,columns=['time','kwh']) #  data= pandas.DataFrame(year,columns=['cups','time','kwh'])
    data.set_index('time', inplace=True, drop=True)
    fig = plt.figure()
    ax = plt.plot(data)
    plt.xticks(rotation='vertical')
    plt.show()
    fig.savefig("entropias.pdf", bbox_inches='tight')


def seriesDataAnalisis(year, freq):
    descomposition = sm.tsa.seasonal_decompose(year,model='multiplicative', freq=freq)
    fig = descomposition.plot()
    matplotlib.rcParams['figure.figsize']=[9.0, 5.0]
    plt.show()
    fig.savefig("teleco_timeSeries.pdf", bbox_inches='tight')
def predictval(df):
    p = d = q = range(0, 2)
    # Generate all different combinations of p, q and q triplets
    pdq = list(itertools.product(p, d, q))

    # Generate all different combinations of seasonal p, q and q triplets
    seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]

    print('Examples of parameter combinations for Seasonal ARIMA...')
    print('SARIMAX: {} x {}'.format(pdq[1], seasonal_pdq[1]))
    print('SARIMAX: {} x {}'.format(pdq[1], seasonal_pdq[2]))
    print('SARIMAX: {} x {}'.format(pdq[2], seasonal_pdq[3]))
    print('SARIMAX: {} x {}'.format(pdq[2], seasonal_pdq[4]))

    warnings.filterwarnings("ignore")  # specify to ignore warning messages
    AIC_list = pd.DataFrame({}, columns=['pram', 'param_seasonal', 'AIC'])
    for param in pdq:
        for param_seasonal in seasonal_pdq:
            try:
                mod = sm.tsa.statespace.SARIMAX(df,
                                                order=param,
                                                seasonal_order=param_seasonal,
                                                enforce_stationarity=False,
                                                enforce_invertibility=False)

                results = mod.fit()

                print('ARIMA{}x{} - AIC:{}'.format(param, param_seasonal, results.aic))
                temp = pd.DataFrame([[param, param_seasonal, results.aic]], columns=['pram', 'param_seasonal', 'AIC'])
                AIC_list = AIC_list.append(temp,
                                           ignore_index=True)  # DataFrame append 는 일반 list append 와 다르게 이렇게 지정해주어야한다.
                del temp

            except:
                continue

    m = np.amin(AIC_list['AIC'].values)  # Find minimum value in AIC
    l = AIC_list['AIC'].tolist().index(m)  # Find index number for lowest AIC
    Min_AIC_list = AIC_list.iloc[l, :]

    print("### Min_AIC_list ### \n{}".format(Min_AIC_list))


def add_freq(idx, freq=None):
    """Add a frequency attribute to idx, through inference or directly.

    Returns a copy.  If `freq` is None, it is inferred.
    """

    idx = idx.copy()
    if freq is None:
        if idx.freq is None:
            freq = pd.infer_freq(idx)
        else:
            return idx
    idx.freq = pd.tseries.frequencies.to_offset(freq)
    if idx.freq is None:
        raise AttributeError('no discernible frequency found to `idx`.  Specify'
                             ' a frequency string with `freq`.')
    return idx

def testPrediction(df):
    df = df[~df.index.duplicated()]
    df=df.asfreq('h')

    mod = sm.tsa.statespace.SARIMAX(df,
                                    order=[1,0,1],
                                    seasonal_order=[1, 1, 1, 12],
                                    enforce_stationarity=False,
                                    enforce_invertibility=False)

    results = mod.fit()

    # Get forecast 500 steps ahead in future
    pred_uc = results.get_forecast(steps=100)

    # Get confidence intervals of forecasts
    pred_ci = pred_uc.conf_int()
    ax = df.plot(label='observed', figsize=(20, 15))
    pred_uc.predicted_mean.plot(ax=ax, label='Forecast')
    ax.fill_between(pred_ci.index,
                    pred_ci.iloc[:, 0],
                    pred_ci.iloc[:, 1], color='k', alpha=.25)
    ax.set_xlabel('Date')
    ax.set_ylabel('Electric consumption')

    plt.legend()
    plt.show()
def compareTimeSeries(serie1,serie2):
    data =  []
    window = []
    for x in range(0, serie1['kwh'].__len__(), 1):
        window = []
        fecha = serie1['Fecha'][x].split('/') # dia, mes, anio
        time = datetime.datetime(int(fecha[2]), int(fecha[1]), int(fecha[0]))  # anio mes dia hora
        window.append(time)
        window.append(serie1['kwh'][x])
        data.append(window)
    serie1 = [i[1] for i in data]
    data =  []
    for x in range(0, serie2['kwh'].__len__(), 1):
        window = []
        fecha = serie2['Fecha'][x].split('/') # dia, mes, anio
        time = datetime.datetime(int(fecha[2]), int(fecha[1]), int(fecha[0]))  # anio mes dia hora
        window.append(time)
        window.append(serie2['kwh'][x])
        data.append(window)
    serie2 = [i[1] for i in data]
    time = [i[0] for i in data]
    print(time.__sizeof__())
    print(serie2.__sizeof__())
    dist, cost, path = dtwco.dtw(serie1, serie2, constraint='itakura', dist_only=False)
    print(cost.shape[0])
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    plot1 = plt.imshow(cost.T, origin='lower', cmap=cm.gray,interpolation='nearest')
    plot2 = plt.plot(path[0], path[1], 'w')

    xlim = ax.set_xlim((-0.5, cost.shape[0] - 0.5))
    ylim = ax.set_ylim((-0.5, cost.shape[1] - 0.5))
    plt.show()
    fig.savefig("comparacion.pdf", bbox_inches='tight')

def seasonalESD(dataset):
    newdata = dataset.values
    print(dataset.index.values)
    test = [i[0] for i in newdata]
    #print(test)
    #print(test)
    outliers_indices = sesd.seasonal_esd(test, hybrid=True,seasonality=168,max_anomalies=600,alpha=0.03)
    i=0
    for idx in outliers_indices:
        print ('{2}. Anomaly time: {0}, anomaly value: {1}'.format(dataset.index.values[idx], test[idx],i))
        i=i+1
    #plt.plot_date(dataset,'-gD',markevery=outliers_indices)
    fig = plt.figure(1)
    plt.plot_date(dataset.index.values,test,'-gD',markevery=outliers_indices)
 #   plt.plot(test,'-gD',markevery=outliers_indices)
    plt.show()
    fig.savefig("anomalias2.pdf", bbox_inches='tight')